package com.huutv.server.event;

import com.huutv.server.model.Category;
import com.huutv.server.model.Post;

import java.util.List;

public interface DataListener {

    void data(Object data);
    void err(String messager);
}
