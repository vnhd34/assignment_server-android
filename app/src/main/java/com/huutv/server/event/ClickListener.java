package com.huutv.server.event;

/**
 * Created by Le Quoc Dat on 20/06/2018.
 */

public interface ClickListener {
    void clickItemListener(int position);
    void longItemListener(int position);
}
