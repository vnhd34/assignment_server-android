package com.huutv.server.util;

import android.app.Activity;
import android.net.Uri;
import android.util.DisplayMetrics;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.common.ResizeOptions;
import com.facebook.imagepipeline.request.ImageRequest;
import com.facebook.imagepipeline.request.ImageRequestBuilder;



public class ImageHelper {

    public static void loadImage(String url, SimpleDraweeView simpleDraweeView, int width, int height) {
        Uri avatarURI = Uri.parse(url);

        ImageRequest request = ImageRequestBuilder.newBuilderWithSource(avatarURI)
                .setResizeOptions(new ResizeOptions(width, height))
                .build();

        simpleDraweeView.setController(
                Fresco.newDraweeControllerBuilder()
                        .setAutoPlayAnimations(true)
                        .setOldController(simpleDraweeView.getController())
                        .setImageRequest(request)
                        .build());
    }



    public static void loadImage(Activity activity, String url, SimpleDraweeView simpleDraweeView, int height) {
        Uri avatarURI = Uri.parse(url);

        ImageRequest request = ImageRequestBuilder.newBuilderWithSource(avatarURI)
                .setResizeOptions(new ResizeOptions(getDeviceMetrics(activity)[0], height))
                .build();

        simpleDraweeView.setController(
                Fresco.newDraweeControllerBuilder()
                        .setOldController(simpleDraweeView.getController())
                        .setAutoPlayAnimations(true)
                        .setImageRequest(request)
                        .build());
    }



    public static int[] getDeviceMetrics(Activity activity) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        int width = displayMetrics.widthPixels;

        return new int[] { width, height };
    }
}
