package com.huutv.server.rest;


import com.huutv.server.util.IP;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


/**
 * Created by Le Quoc Dat on 20/06/2018.
 */

public class APIConfig {

    public static final String BASE_URL = IP.URL;
    private static Retrofit retrofit = null;

    public static APIService getClient() {
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return APIConfig.retrofit.create(APIService.class);
    }
}
