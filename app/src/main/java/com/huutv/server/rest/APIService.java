package com.huutv.server.rest;

import com.huutv.server.model.Category;
import com.huutv.server.model.Post;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;


/**
 * Created by Le Quoc Dat on 20/06/2018.
 */

public interface APIService {

    @GET("/API/CATEGORY/0")
    Call<List<Category>> getCategory();

    @GET("/API/POST/0")
    Call<List<Post>> getPosts();
}
