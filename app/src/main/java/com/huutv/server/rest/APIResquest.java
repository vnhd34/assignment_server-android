package com.huutv.server.rest;

import android.os.AsyncTask;

import com.huutv.server.event.DataListener;
import com.huutv.server.model.Assignment;
import com.huutv.server.model.Category;
import com.huutv.server.model.Post;

import java.util.List;

import retrofit2.Response;

public class APIResquest {


    public static class API extends AsyncTask<Void, Void, Assignment>{
        private DataListener mListener;

        public API(DataListener mListener) {
            this.mListener = mListener;
        }

        @Override
        protected Assignment doInBackground(Void... voids) {
            try{
                Response<List<Post>> post = APIConfig.getClient().getPosts().execute();
                Response<List<Category>> category = APIConfig.getClient().getCategory().execute();
                if(post.code() != 404 && category.code() != 404){
                    return new Assignment(post.body(), category.body());
                }
            }catch (Exception e){
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Assignment posts) {
            super.onPostExecute(posts);
            if (posts != null){
                mListener.data(posts);
                return;
            }
            mListener.err("Erro");
        }
    }

    public static class APIPOST extends AsyncTask<Void, Void, List<Post>>{
        private DataListener mListener;

        public APIPOST(DataListener mListener) {
            this.mListener = mListener;
        }

        @Override
        protected List<Post> doInBackground(Void... voids) {
            try{
                Response<List<Post>> response = APIConfig.getClient().getPosts().execute();
                if(response.code() != 404){
                    return response.body();
                }
            }catch (Exception e){
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(List<Post> posts) {
            super.onPostExecute(posts);
            if (posts != null){
                mListener.data(posts);
                return;
            }
            mListener.err("Erro");
        }
    }

    public static class APTCategory extends AsyncTask<Void, Void, List<Category>>{
        private DataListener mListener;

        public APTCategory(DataListener mListener) {
            this.mListener = mListener;
        }
        @Override
        protected List<Category> doInBackground(Void... voids) {
            try{
                Response<List<Category>> response = APIConfig.getClient().getCategory().execute();
                if(response.code() != 404){
                    return response.body();
                }
            }catch (Exception e){
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(List<Category> categories) {
            super.onPostExecute(categories);
            if (categories != null){
                mListener.data(categories);
                return;
            }
            mListener.err("Erro");
        }
    }
}
