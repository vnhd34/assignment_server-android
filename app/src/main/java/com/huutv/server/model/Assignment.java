package com.huutv.server.model;

import java.util.List;

public class Assignment {
    private List<Post> mPost;
    private List<Category> mCategory;

    public Assignment(List<Post> post,  List<Category> category) {
        this.mPost = post;
        this.mCategory = category;
    }

    public List<Post> getmPost() {
        return mPost;
    }

    public void setmPost(List<Post> mPost) {
        this.mPost = mPost;
    }

    public List<Category> getmCategory() {
        return mCategory;
    }

    public void setmCategory(List<Category> mCategory) {
        this.mCategory = mCategory;
    }
}
