package com.huutv.server.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.huutv.server.R;
import com.huutv.server.event.ClickListener;
import com.huutv.server.ui.custom.CircleProgressDrawable;
import com.huutv.server.util.IP;
import com.huutv.server.util.ImageHelper;
import com.huutv.server.model.Post;

import java.util.List;

public class AdapterPosts extends RecyclerView.Adapter<AdapterPosts.ViewHolder> {

    private List<Post> mPostsList;
    private ClickListener mListener;



    public AdapterPosts(List<Post> postsList, ClickListener clickListener) {
        mPostsList = postsList;
        mListener = clickListener;
    }



    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_item_post, parent, false);
        return new ViewHolder(view);
    }



    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.bindData(mPostsList.get(position));
    }



    @Override
    public int getItemCount() {
        return mPostsList.size();
    }



    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private SimpleDraweeView mThumbnail;
        private TextView mTitle, mAuthor, mCategory;



        public ViewHolder(View itemView) {
            super(itemView);

            mThumbnail = itemView.findViewById(R.id.thumbnailImageview);
            mTitle = itemView.findViewById(R.id.titleTextview);
            mAuthor = itemView.findViewById(R.id.authorTextview);
            mCategory = itemView.findViewById(R.id.categoryTextview);

            itemView.setOnClickListener(this);
        }



        public void bindData(Post posts) {
            mThumbnail.setClipToOutline(true);
            String image = IP.URL + posts.getImage();
            Log.d("image", image);
            ImageHelper.loadImage(image, mThumbnail, 150, 100);
            mThumbnail.getHierarchy().setProgressBarImage(new CircleProgressDrawable());

            mTitle.setText(posts.getTitle());
            mAuthor.setText(posts.getAuthor());
            mCategory.setText(posts.getCategory());
        }



        @Override
        public void onClick(View view) {
            if (mListener != null)
                mListener.clickItemListener(getAdapterPosition());
        }
    }
}
