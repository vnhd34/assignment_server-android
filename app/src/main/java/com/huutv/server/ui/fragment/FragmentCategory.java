package com.huutv.server.ui.fragment;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.huutv.server.R;
import com.huutv.server.event.ClickListener;
import com.huutv.server.event.DataListener;
import com.huutv.server.model.Category;
import com.huutv.server.model.Post;
import com.huutv.server.rest.APIResquest;
import com.huutv.server.ui.activity.DetailPostActivity;
import com.huutv.server.ui.adapter.AdapterPosts;

import java.util.List;


@SuppressLint("ValidFragment")
public class FragmentCategory extends Fragment implements ClickListener {

    private AdapterPosts mAdapterPosts;
    private RecyclerView mRecyclerView;
    private List<Post> mName;


    public FragmentCategory(List<Post> name) {
        mName = name;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_category, container, false);
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mRecyclerView = view.findViewById(R.id.recyclerPosts);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        getPosts();
    }


    private void getPosts() {
        List<Post> getPosts = mName;
        mAdapterPosts = new AdapterPosts(getPosts, this);
        mRecyclerView.setAdapter(mAdapterPosts);
    }

    @Override
    public void clickItemListener(int position) {
        Intent intent = new Intent(getActivity(), DetailPostActivity.class);
        intent.putExtra("idPost", mName.get(position));
        startActivity(intent);
    }

    @Override
    public void longItemListener(int position) {

    }
}
