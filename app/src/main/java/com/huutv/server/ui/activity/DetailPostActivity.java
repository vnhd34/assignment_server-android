package com.huutv.server.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.huutv.server.R;
import com.huutv.server.ui.custom.CircleProgressDrawable;
import com.huutv.server.util.IP;
import com.huutv.server.util.ImageHelper;
import com.huutv.server.model.Post;

import java.util.Objects;


/**
 * Created by Le Quoc Dat on 07/08/2018.
 */

public class DetailPostActivity extends AppCompatActivity {
    int[] mDriver;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_post);
        findViewById(R.id.backImageView).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        mDriver = ImageHelper.getDeviceMetrics(this);
        Intent intent = getIntent();
        Post post = Objects.requireNonNull(intent.getExtras()).getParcelable("idPost");
        if (post != null) {
            SimpleDraweeView imageView = findViewById(R.id.thumbnail);
            ImageHelper.loadImage(IP.URL + post.getImage(), imageView, mDriver[0], mDriver[1] / 2);
            imageView.getHierarchy().setProgressBarImage(new CircleProgressDrawable());

            TextView titleTextview = findViewById(R.id.titleTextview);
            titleTextview.setText(post.getTitle());

            TextView authorTextview = findViewById(R.id.authorTextview);
            authorTextview.setText(post.getAuthor());

            TextView content = findViewById(R.id.contentTextview);
            content.setText(post.getContent());
        }
    }
}
