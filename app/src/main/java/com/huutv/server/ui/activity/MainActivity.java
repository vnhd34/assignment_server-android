package com.huutv.server.ui.activity;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.huutv.server.R;

import com.huutv.server.event.DataListener;
import com.huutv.server.model.Assignment;
import com.huutv.server.model.Category;
import com.huutv.server.model.Post;
import com.huutv.server.rest.APIResquest;
import com.huutv.server.ui.fragment.FragmentCategory;
import com.huutv.server.ui.adapter.MainPagerAdapter;

import java.util.ArrayList;
import java.util.List;


public class MainActivity extends AppCompatActivity implements DataListener {

    private MainPagerAdapter mAdapter;
    private ViewPager mViewPager;
    private TabLayout mTabLayout;
    private ProgressDialog mProgress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Fresco.initialize(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();
        new APIResquest.API(this).execute();
    }

    private void initView() {
        mViewPager = findViewById(R.id.viewpager);
        mTabLayout = findViewById(R.id.viewpagertab);
        mProgress = new ProgressDialog(this);
        mProgress.setMessage("Loadding");
        mProgress.show();
    }


    @Override
    public void data(Object data) {
        if(data == null ){
            return;
        }
        mProgress.dismiss();
        Assignment assignment = (Assignment) data;
        List<Category> categories = assignment.getmCategory();
        List<Post> post = assignment.getmPost();
        List<Fragment> fragments = new ArrayList<>();
        List<String> title = new ArrayList<>();
        fragments.add(new FragmentCategory(post));
        title.add("NEW");
        for (int i = 0; i < categories.size(); i++) {
            List<Post> postsCategory = new ArrayList<>();
            for (int x = 0; x < post.size(); x++){
                if(categories.get(i).getName().equalsIgnoreCase(post.get(x).getCategory())){
                    postsCategory.add(post.get(x));
                }
            }
            Fragment fragmentCategory = new FragmentCategory(postsCategory);
            fragments.add(fragmentCategory);
            title.add(categories.get(i).getName());
        }
        mAdapter = new MainPagerAdapter(getSupportFragmentManager(), fragments, title);
        mViewPager.setAdapter(mAdapter);
        mViewPager.setOffscreenPageLimit(categories.size());
        mTabLayout.setupWithViewPager(mViewPager);
    }

    @Override
    public void err(String messager) {

    }
}
